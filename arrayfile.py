from pickletools import uint2
import matplotlib
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors
#from pathlib import Path
import argparse
dt=np.uint8
data=np.fromfile('.\data\gl-latlong-1km-landcover.bsq',dtype=dt)
plot_data=data.reshape((21600,43200))
plt.imshow(plot_data[::50,::50])
plt.show()
maryland_colors = np.array([
    ( 68,  79, 137),
    (  1, 100,   0),
    (  1, 130,   0),
    (151, 191,  71),
    (  2, 220,   0),
    (  0, 255,   0),
    (146, 174,  47),
    (220, 206,   0),
    (255, 173,   0),
    (255, 251, 195),
    (140,  72,   9),
    (247, 165, 255),
    (255, 199, 174),
    (  0, 255, 255),])
# maryland_cmap = colors.ListedColormap(maryland_colors)
# image = plt.imshow(plot_data, cmap=maryland_cmap) 
# plt.show()
parser = argparse.ArgumentParser()
parser.add_argument("coordinate", help="display geographical position of input coordinates",
                    type=int)
args = parser.parse_args()
print(args.coordinate)